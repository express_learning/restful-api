const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const ejs=require("ejs");
//const _=require("lodash");
const app=express();

app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static("public")); //css file was not applied without this

mongoose.connect("mongodb://127.0.0.1:27017/wikiDB")
const articleSchema={
    title: String,
    content: String
};

const Article = mongoose.model("Article",articleSchema);

app.get("/articles",async function(req,res){
   try{
    await Article.find({}).then(function(items){
        res.send(items);
     })
   }catch(error){
    res.send(error);
   }
});

app.post("/articles",function(req,res){
    const newArticle= new Article({
        title:req.body.title,
        content:req.body.content
    })
    newArticle.save().then(success=>{
        res.send("Successfully added")
    }).catch(error=>{
        res.send(error)
    });
})

app.delete("/articles",function(req,res){
    Article.deleteMany().then(succ=>{
        res.send("Successfully deleted")
    }).catch(err=>{
        res.send(err)
    })
})

app.route("/articles/:atit")

.get(function(req,res){
    Article.findOne({title:req.params.atit}).then(article=>{
        if (article) {
            res.send(article)
        } else {
            res.send("No article found with the name")
        }
    })
})

.put(function(req,res){
    Article.updateOne(
        {title:req.params.atit},
        {title:req.body.title, content:req.body.content}
    ).then(succ=>{
        res.send("Successfully updated article")
    })
})

.patch(function(req,res){
    Article.updateOne(
        {title:req.params.atit},
        {$set: req.body}
    ).then(succ=>{
        res.send("Successfully updated article")
    })
})
.delete(function(req,res){
    Article.deleteOne(
           {title:req.params.atit}
    ).then(succ=>{
        res.send("Successfully deleted the article")
    })
});
app.listen(3000,()=>{
    console.log("Server started on port 3000");
})